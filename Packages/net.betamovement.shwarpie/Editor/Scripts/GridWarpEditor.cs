﻿using UnityEditor;
using UnityEngine;

namespace Shwarpie
{
	[CustomEditor(typeof(GridWarp))]
	public class GridWarpEditor
		: Editor
	{
		private bool _expandVerts;

		private SerializedProperty _editToggleKeyProp;

		private SerializedProperty _translateStepAmountProp;
		private SerializedProperty _translateNudgeAmountProp;

		private SerializedProperty _rotateStepAmountProp;
		private SerializedProperty _rotateNudgeAmountProp;

		private SerializedProperty _scaleStepAmountProp;
		private SerializedProperty _scaleNudgeAmountProp;
		
		private SerializedProperty _dataFileProp;
		private SerializedProperty _handlePrefabProp;

		public void OnEnable()
		{
			_editToggleKeyProp = serializedObject.FindProperty("editToggleKey");

			_translateStepAmountProp = serializedObject.FindProperty("translateStepAmount");
			_translateNudgeAmountProp = serializedObject.FindProperty("translateNudgeAmount");

			_rotateStepAmountProp = serializedObject.FindProperty("rotateStepAmount");
			_rotateNudgeAmountProp = serializedObject.FindProperty("rotateNudgeAmount");

			_scaleStepAmountProp = serializedObject.FindProperty("scaleStepAmount");
			_scaleNudgeAmountProp = serializedObject.FindProperty("scaleNudgeAmount");

			_dataFileProp = serializedObject.FindProperty("dataFile");
			_handlePrefabProp = serializedObject.FindProperty("handlePrefab");
		}

		public override void OnInspectorGUI()
		{
			serializedObject.Update();

			var gridWarp = (target as GridWarp);

			using (new EditorGUILayout.VerticalScope("box"))
			{
				EditorGUILayout.PropertyField(_editToggleKeyProp);
				EditorGUILayout.Space();
				EditorGUILayout.PropertyField(_translateStepAmountProp);
				EditorGUILayout.PropertyField(_translateNudgeAmountProp);
				EditorGUILayout.Space();
				EditorGUILayout.PropertyField(_rotateStepAmountProp);
				EditorGUILayout.PropertyField(_rotateNudgeAmountProp);
				EditorGUILayout.Space();
				EditorGUILayout.PropertyField(_scaleStepAmountProp);
				EditorGUILayout.PropertyField(_scaleNudgeAmountProp);
			}

			if (gridWarp.Editing)
			{
				GUI.color = Color.green;
				if (GUILayout.Button("Lock Warp"))
				{
					gridWarp.Editing = false;
					EditorApplication.QueuePlayerLoopUpdate();
				}
				GUI.color = Color.red;
				if (GUILayout.Button("Reset"))
				{
					gridWarp.ResetVertices();
					EditorApplication.QueuePlayerLoopUpdate();
				}
				GUI.color = Color.white;
			}
			else
			{
				GUI.color = Color.green;
				if (GUILayout.Button("Edit Warp"))
				{
					gridWarp.Editing = true;
					EditorApplication.QueuePlayerLoopUpdate();
				}
				GUI.color = Color.white;
			}

			using (new EditorGUI.DisabledScope(!gridWarp.Editing))
			{
				EditorGUILayout.PropertyField(_handlePrefabProp);

				gridWarp.NumCols = EditorGUILayout.IntSlider("Cols", gridWarp.NumCols, 2, 8);
				gridWarp.NumRows = EditorGUILayout.IntSlider("Rows", gridWarp.NumRows, 2, 8);

				_expandVerts = EditorGUILayout.Foldout(_expandVerts, "Vertices");
				if (_expandVerts)
				{
					for (int i = 0; i < gridWarp.Vertices.Count; ++i)
					{
						const float kHeaderSize = 40.0f;

						using (new EditorGUILayout.HorizontalScope())
						{
							GUILayout.Label(string.Format("[{0:0}, {1:0}]", gridWarp.Vertices[i].coord.x, gridWarp.Vertices[i].coord.y), GUILayout.Width(kHeaderSize));
							EditorGUILayout.ObjectField(gridWarp.Vertices[i].go, typeof(GameObject), true);
						}
					}
				}

				if (gridWarp.Editing)
				{
					EditorApplication.QueuePlayerLoopUpdate();
				}
			}

			EditorGUILayout.PropertyField(_dataFileProp);

			using (new EditorGUI.DisabledScope(string.IsNullOrEmpty(gridWarp.dataFile) || !gridWarp.Editing))
			{
				GUI.color = Color.green;
				if (GUILayout.Button("Save Data"))
				{
					gridWarp.SaveData();
					EditorApplication.QueuePlayerLoopUpdate();
				}
				GUI.color = Color.red;
				if (GUILayout.Button("Load Data"))
				{
					gridWarp.LoadData();
					EditorApplication.QueuePlayerLoopUpdate();
				}
			}

			serializedObject.ApplyModifiedProperties();
		}
	}
}