## [1.1.1](https://gitlab.com/betamovement/shwarpie/compare/v1.1.0...v1.1.1) (2021-10-04)


### Bug Fixes

* **package:** fix assembly definition namespace ([5746858](https://gitlab.com/betamovement/shwarpie/commit/5746858d9b718048f502247d6232e46badbe93bb))

# [1.1.0](https://gitlab.com/betamovement/shwarpie/compare/v1.0.0...v1.1.0) (2021-02-10)


### Bug Fixes

* **ci:** fix name in `package.json` ([eede165](https://gitlab.com/betamovement/shwarpie/commit/eede165c2c0b542a78010f0066baf30e336639e6))


### Features

* **package:** rename all "WarpSharp" references to "Shwarpie" ([29df3b5](https://gitlab.com/betamovement/shwarpie/commit/29df3b5dbe58961a0052b8654d183d00e1365dae))

# 1.0.0 (2021-02-10)


### Bug Fixes

* **package:** add scoped registry entry for JSON lib ([b13c9c2](https://gitlab.com/betamovement/shwarpie/commit/b13c9c264d8aaadd8ab9c35ff95613255aac6fe0))
* **package:** tag editor assembly correctly ([4763f6d](https://gitlab.com/betamovement/shwarpie/commit/4763f6d928c6dab2a52a52428b775cadfbbf293b))
* **samples:** rename example scene ([2793706](https://gitlab.com/betamovement/shwarpie/commit/27937067e02336ddff8dd58f3acdc3562517b860))
* **warp:** adjust material and ensure handles are on the same layer as parent ([cf1b67f](https://gitlab.com/betamovement/shwarpie/commit/cf1b67f629f7272827efec6e6f119825de4cf77e))
* **warp:** make handles bigger ([ea52698](https://gitlab.com/betamovement/shwarpie/commit/ea526983381ea34d66a94c0617ebaee9851fb152))


### Features

* **package:** add assembly definition files ([41199c2](https://gitlab.com/betamovement/shwarpie/commit/41199c2764e86414a8b9a0f36e88dda87d613d5e))
* **package:** add package file for UPM, let's see if this works... ([e7ff0bf](https://gitlab.com/betamovement/shwarpie/commit/e7ff0bf71a822acda227c3fbbe1af865dee240ca))
* **package:** update namespace to "WarpSharp" ([7433e89](https://gitlab.com/betamovement/shwarpie/commit/7433e891c41efecb7d6ee41aa2a0e22ee3c8b54d))
* **serialization:** save serialization files in a "GridWarp" subfolder in SA ([2687b5c](https://gitlab.com/betamovement/shwarpie/commit/2687b5cb0f146aa4d71ef673bb505ae19afa7169))
* **serialization:** switch to JSON for serialization, I don't know why I bother with anything built-in in Unity... ([fa05352](https://gitlab.com/betamovement/shwarpie/commit/fa0535255e99a0b3bb8dfbe4ee3c11fb38e433ff))
* **warp:** rotate and scale from center ([c5bc9fc](https://gitlab.com/betamovement/shwarpie/commit/c5bc9fcac9653e80eb7349bab461707b424a68f0))
* **warp:** update `GridWarp` controls to allow multi-selection, rotation and scale! ([25d21e1](https://gitlab.com/betamovement/shwarpie/commit/25d21e1eb2fe502c376a6f739ac8890a4d821663))
