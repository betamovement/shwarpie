# About Shwarpie

Unity tool for generating warp meshes used in projection mapping.

* Mesh grid can have any number of rows and columns.
* Mesh can be edited in Editor or in Game using fun keyboard shortcuts.
* Settings are serialized to JSON and saved to disk in the Streaming Assets folder.
* More coming soon... 

# Installing Shwarpie

Compatible with the [Unity Package Manager](https://docs.unity3d.com/2020.2/Documentation/Manual/Packages.html).

Add scope for `net.betamovement`, then add the package in the list of dependencies. Your `<project>/Packages/manifest.json` should look like this:

```
{
  "scopedRegistries": [
    {
      "name": "Beta Movement",
      "url": "https://registry.betamovement.net/",
      "scopes": ["net.betamovement"]
    }
  ],
  "dependencies": {
    "net.betamovement.shwarpie": "1.0.0",
    // ...
  }
}
```

# Using Shwarpie

Documentation coming soon. In the meantime, please refer to the example scene.

# Technical details

## Requirements

This version of Shwarpie is compatible with the following versions of the Unity Editor:

* 2019.4 and later (recommended)
